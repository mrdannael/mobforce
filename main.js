const express = require('express');
const http = require('http');
const path = require('path');
const port = process.env.PORT || 8080;
const app = express();
const socketIO = require('socket.io')

app.use(express.static(__dirname + '/dist'));

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'index.html'));
});

const server = http.createServer(app);

const io = socketIO.listen(server);

var players = {};

io.on('connection', (socket) => {
    console.log('a user connected!');
    players[socket.id] = {
        rotation: 0,
        x: Math.floor(Math.random() * 700) + 50,
        y: Math.floor(Math.random() * 500) + 50,
        playerId: socket.id,
    };
    socket.emit('currentPlayers', players);
    socket.broadcast.emit('newPlayer', players[socket.id]);
    socket.on('disconnect', function() {
        console.log('user disconnected!');
        delete players[socket.id];
        io.emit('disconnect', socket.id);
    });
});

server.listen(port, () => console.log(`Listening on port ${port}`));