import Phaser from 'phaser';
import logoImg from './../assets/logo.png';
import blueButton1 from './../assets/ui/blue_button02.png';
import blueButton2 from './../assets/ui/blue_button03.png';
import box from './../assets/ui/grey_box.png';
import checkedBox from './../assets/ui/blue_boxCheckmark.png';
import bgMusic from './../assets/TownTheme.mp3';
import player from './../assets/player.png';
import enemy from './../assets/enemy.png';
import crate from './../assets/crate.png';
import bullet from './../assets/bullet.png';
import gun from './../assets/gun.png';
import tiles from './../assets/tileset.png';
import map from './../assets/tileset.json';

export default class PreloaderScene extends Phaser.Scene {
    constructor() {
        super('Preloader');
    }

    preload() {
        this.add.image(400, 200, 'zenva_logo');

        var progressBar = this.add.graphics();
        var progressBox = this.add.graphics();
        progressBox.fillStyle(0x222222, 0.8);
        progressBox.fillRect(240, 270, 320, 50);

        var width = this.cameras.main.width;
        var height = this.cameras.main.height;
        var loadingText = this.make.text({
            x: width / 2,
            y: height / 2 - 50,
            text: 'Loading',
            style: {
                font: '20px monospace',
                fill: '#ffffff',
            }
        });
        loadingText.setOrigin(0.5, 0.5);

        var percentText = this.make.text({
            x: width / 2,
            y: height / 2 - 5,
            text: '0%',
            style: {
                font: '10px monospace',
                fill: '#ffffff',
            }
        });
        percentText.setOrigin(0.5, 0.5);

        var assetText = this.make.text({
            x: width / 2,
            y: height / 2 + 50,
            text: '',
            style: {
                font: '10px monospace',
                fill: '#ffffff',
            }
        });
        assetText.setOrigin(0.5, 0.5);

        this.load.on('progress', (value) => {
            percentText.setText(parseInt(value * 100) + '%');
            progressBar.clear();
            progressBar.fillStyle(0xffffff, 1);
            progressBar.fillRect(250, 280, 300 * value, 30);
        });

        this.load.on('fileprogress', (file) => {
            assetText.setText('Loading asset: ' + file.key);
        })

        this.load.on('complete', function () {
            progressBox.destroy();
            progressBar.destroy();
            loadingText.destroy();
            percentText.destroy();
            assetText.destroy();
            this.ready();
        }.bind(this));

        this.timedEvent = this.time.delayedCall(3000, this.ready, [], this);

        this.load.image('blueButton1', blueButton1);
        this.load.image('blueButton2', blueButton2);
        this.load.image('phaserLogo', logoImg);
        this.load.image('box', box);
        this.load.image('checkedBox', checkedBox);
        this.load.audio('bgMusic', [bgMusic]);
        this.load.image('player', player);
        this.load.image('enemy', enemy);
        this.load.image('crate', crate);
        this.load.image('bullet', bullet);
        this.load.image('gun', gun);
        this.load.image('tiles', tiles);
        this.load.tilemapTiledJSON('map', map);
    }

    create() {

    }

    init() {
        this.readyCount = 0;
    }

    ready() {
        this.scene.start('Game');
        this.readyCount++;
        if (this.readyCount === 2) {
            this.scene.start('Title');
        }
    }
};
