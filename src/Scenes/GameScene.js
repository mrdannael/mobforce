import Phaser from 'phaser';
import Bullet from './../Objects/Bullet';
import Player from './../Objects/Player';
import Enemy from '../Objects/Enemy';
import Crate from '../Objects/Crate';

export default class GameScene extends Phaser.Scene {
    constructor() {
        super('Game');
    }

    preload() {}

    create() {
        this.map = this.make.tilemap({ key: 'map' });
        this.tileset = this.map.addTilesetImage('tileset', 'tiles');
        this.ground = this.map.createStaticLayer('ground', this.tileset, 0, 0);
        this.rails = this.map.createStaticLayer('rails', this.tileset, 0, 0);

        const camera = this.cameras.main;

        // Set up the arrows to control the camera
        const cursors = this.input.keyboard.createCursorKeys();
        this.controls = new Phaser.Cameras.Controls.FixedKeyControl({
            camera: camera,
            left: cursors.left,
            right: cursors.right,
            up: cursors.up,
            down: cursors.down,
            speed: 0.5
        });

        // Constrain the camera so that it isn't allowed to move outside the width/height of tilemap
        camera.setBounds(0, 0, this.map.widthInPixels, this.map.heightInPixels);

        this.cameras.main.backgroundColor.setTo(255,255,255);

        this.bullets = this.physics.add.group({ name: 'bullets' });
        this.enemies = this.physics.add.group({ name: 'enemies' });
        this.crates = this.physics.add.group({ name: 'crates' });

        this.player = new Player(this);
        camera.startFollow(this.player);

        new Enemy(this);
        new Crate(this);

        this.input.on('pointerdown', function(pointer) {
            new Bullet(this, pointer);
        }.bind(this));
    }

    update(time, delta) {
        this.controls.update(delta);
    }
};
