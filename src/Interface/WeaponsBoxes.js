import Phaser from 'phaser';

export default class WeaponsBoxes extends Phaser.GameObjects.Container {
    constructor(scene) {
        super(scene);

        this.depth = 9999;
        this.scene = scene;

        this.weapon1 = this.createWeaponBox(0, 0, 1);
        this.weapon2 = this.createWeaponBox(0, 45, 2);
        this.weapon3 = this.createWeaponBox(0, 90, 3);
        this.weapon4 = this.createWeaponBox(0, 135, 4);
        
        scene.events.on('update', this.update, this);

        this.scene.input.keyboard.on('keydown', function(event) {
            switch (event.key) {
                case "1":
                    this.changeActiveWeapon(this.weapon1);
                    break;
                case "2":
                    this.changeActiveWeapon(this.weapon2);
                    break;
                case "3":
                    this.changeActiveWeapon(this.weapon3);
                    break;
                case "4":
                    this.changeActiveWeapon(this.weapon4);
                    break;
                default:
                    break;
            }
        }.bind(this))
    }

    update() {}

    createWeaponBox(x, y, count) {
        var temp, graphics;
        temp = new Phaser.Geom.Rectangle(x, y, 125, 40);
        graphics = this.scene.add.graphics({
            fillStyle: {
                color: 0x222222,
                alpha: 0.2
            }
        });
        graphics.fillRectShape(temp);
        this.scene.make.text({
            x: x + 5,
            y: y + 10,
            text: count,
            style: {
                font: '20px monospace',
                fill: '#ffffff'
            }
        })
        return temp;
    }

    changeActiveWeapon(weapon) {
        console.log(weapon)
    }
}