import Phaser from 'phaser';

export default class Player extends Phaser.GameObjects.Sprite {
    constructor(scene) {
        super(scene, 200, 200, 'enemy');

        this.health = 100;

        scene.enemies.add(this);
        scene.add.existing(this);

        scene.physics.world.enable(this);
        this.body.setCollideWorldBounds(true);
        this.body.setCircle(16);
        this.body.setImmovable(true);

        scene.physics.add.collider(this, scene.bullets, function(enemy, bullet) {
            enemy.health -= 25;
            bullet.destroy();
        });

        scene.events.on('update', this.update, this);
    }

    update() {
        if (this.health <= 0) {
            this.destroy();
        }
    }
}