import Phaser from 'phaser';

export default class Bullet extends Phaser.GameObjects.Sprite {
    constructor(scene, pointer) {
        const { player: { x, y }} = scene;
        super(scene, x, y, 'bullet');

        scene.physics.world.enable(this);
        scene.bullets.add(this);
        scene.add.existing(this);
        
        scene.physics.moveTo(this, pointer.x, pointer.y, 600);
    }
}