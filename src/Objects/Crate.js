import Phaser from 'phaser';

export default class Crate extends Phaser.GameObjects.Sprite {
    constructor(scene) {
        super(scene, 400, 400, 'crate');

        scene.crates.add(this);
        scene.add.existing(this);

        scene.physics.world.enable(this);
        this.body.setImmovable(true);

        scene.physics.add.collider(scene.player, this, function(player, crate) {});

        scene.physics.add.collider(this, scene.bullets, function(create, bullet) {
            bullet.destroy();
        })

        scene.events.on('update', this.update, this);
    }

    update() {}
}