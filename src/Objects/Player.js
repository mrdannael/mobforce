import Phaser from 'phaser';
// import WeaponsBoxes from './../Interface/WeaponsBoxes';

export default class Player extends Phaser.GameObjects.Container {
    constructor(scene) {
        super(scene, 100, 100)
        this.player = scene.add.sprite(0, 0, 'player');
        this.add(this.player);

        this.gun = scene.add.sprite(18, 0, 'gun');
        this.add(this.gun);
        
        scene.add.existing(this);

        scene.physics.world.enable(this);
        this.body.setCollideWorldBounds(true);
        this.body.setCircle(16, -16, -16);

        scene.physics.add.collider(this, scene.enemies, function(player, enemy) {});

        this.scene = scene;
        this.keys = scene.input.keyboard.addKeys('W,S,A,D');

        scene.events.on('update', this.update, this);

        // new WeaponsBoxes(scene);
    }

    update() {
        this.body.setVelocity(0);

        if (this.keys.W.isDown) {
            this.body.setVelocityY(-80);
        }
        if (this.keys.A.isDown) {
            this.body.setVelocityX(-80);
        }
        if (this.keys.D.isDown) {
            this.body.setVelocityX(80);
        }
        if (this.keys.S.isDown) {
            this.body.setVelocityY(80);
        }

       this.setAngle(Phaser.Math.RAD_TO_DEG * Phaser.Math.Angle.BetweenPoints(this, this.scene.input.mousePointer));        
    }
}